<?php
$stati = json_decode(file_get_contents(__DIR__ . '/../../data/stato.json'));

$statoNuovo =
    [
        'id' => $_REQUEST['id'],
        'userId' => $_REQUEST['userId'],
        'chatId' => $_REQUEST['chatId'],
        'domandaId' => $_REQUEST['domandaId'],
        'punteggio' => $_REQUEST['punteggio']
    ];
$nuoviStati = [];
foreach ($stati as $stato) {
    if ($stato->id == $_REQUEST['id']) {
        $nuoviStati[] = $statoNuovo;
    } else {
        $nuoviStati[] = $stato;
    }
}
file_put_contents(__DIR__ . '/../../data/stato.json', json_encode($nuoviStati));

header('Location: index.php');