<pre><?php
$id = $_REQUEST['id'];

$stati = json_decode(file_get_contents(__DIR__ . '/../../data/stato.json'));


$nuoveStati = [];
foreach ($stati as $stato) {
    if ($stato->id != $id) {
        $nuoveStati[] = $stato;
    }
}
file_put_contents(__DIR__ . '/../../data/stato.json',json_encode($nuoveStati));

header('Location: index.php');