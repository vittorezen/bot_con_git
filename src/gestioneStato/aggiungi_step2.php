<?php
$stati = json_decode(file_get_contents(__DIR__ . '/../../data/stato.json'));
$maxId = -1;
foreach ($stati as $stato) {
    $maxId = max($maxId, $stato->id);
}
$stati[] =
    [
        'id' => ($maxId + 1),
        'userId' => $_REQUEST['userId'],
        'chatId' => $_REQUEST['chatId'],
        'domandaId' => $_REQUEST['domandaId'],
        'punteggio' => $_REQUEST['punteggio']

    ];

file_put_contents(__DIR__ . '/../../data/stato.json', json_encode($stati));

header('Location: index.php');