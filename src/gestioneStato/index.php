<table border="1">
    <tr>
        <td>Id</td>
        <td>UserId</td>
        <td>ChatId</td>
        <td>DomandaId</td>
        <td>Punteggio</td>
        <td>Azioni</td>
    </tr>
    <?php
    if (file_exists(__DIR__ . '/../../data/stato.json')) {
        $stati = json_decode(file_get_contents(__DIR__ . '/../../data/stato.json'));
        foreach ($stati as $stato) {
            echo "<tr>";
            echo "<td>" . $stato->id . "</td>";
            echo "<td>" . $stato->userId . "</td>";
            echo "<td>" . $stato->chatId . "</td>";
            echo "<td>" . $stato->domandaId . "</td>";
            echo "<td>" . $stato->punteggio . "</td>";
            echo "<td>";
            echo "[<a href=\"modifica.php?id=" . $stato->id . "\">Modifica</a>] ";
            echo "[<a href=\"elimina.php?id=" . $stato->id . "\">Elimina</a>]";
            echo "</td>";
            echo "</tr>";
        }
    }
    ?>
</table>
<a href="aggiungi.php">Aggiungi uno stato</a>

