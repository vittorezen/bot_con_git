<?php
require(__DIR__ . '/../../vendor/autoload.php');

use Telegram\Bot\Api;

$telegram = new Api('3099:klòsdj9w8nhmtk');

$updates = $telegram->getUpdates(array());

foreach ($updates as $update) {
    $risposta = analizzaMessaggio($update);
    $response = $telegram->sendMessage($risposta);
    $telegram->getUpdates(array('offset' => $update->getUpdateId() + 1));
}

function analizzaMessaggio($update)
{

    $from = $update->getMessage()->getFrom();
    /*    $msg = "Ciao " . $from['first_name'] . ' ' . $from['last_name'] . ".";
        $risposta = [
            'chat_id' => $update->getMessage()->getChat()['id'],
            'text' => $msg
        ];
    */
    $userId = $update->getMessage()->getFrom()['id'];
    $testoInviato = $update->getMessage()->getText();
    $stato = cercaStato($userId);

    if ($stato == false) {
        $risposta = creaNuovaDomandaPerIlGiocatore($userId);
    } else {
        if (rispostaCorretta($stato,$testoInviato)){
            $risposta=inviaCongratulazioniAlGiocatore($userId);
        } else {
            $risposta=inviaErroreAlGiocatore($userId);
        }
    }
    return $risposta;
}


function cercaStato($userId)
{
    $stati = json_decode(file_get_contents(__DIR__ . '/../../data/stato.json'));
    $stato = false;
    foreach ($stati as $stato) {
        if ($stato->userId == $userId) {
            break;
        }
    }
    return $stato;
}