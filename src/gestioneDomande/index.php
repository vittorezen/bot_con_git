<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
      integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<body>
<div class="container">
    <table class="table table-hover table-zebra">

        <tr>
            <td>Id</td>
            <td>Domanda</td>
            <td>Risposta esatta</td>
            <td>Azioni</td>
        </tr>
        <?php

        $link = mysql_connect('localhost', 'root', 'root')
        or die('Could not connect: ' . mysql_error());
        mysql_select_db('bot') or die('Could not select database');

        $query = 'SELECT * FROM domande';
        $result = mysql_query($query) or die('Query failed: ' . mysql_error());

        while ($domanda = mysql_fetch_array($result, MYSQL_ASSOC)) {
            echo "<tr>";
            echo "<td>" . $domanda['id'] . "</td>";
            echo "<td>" . $domanda['domanda'] . "</td>";
            echo "<td>" . $domanda['risposta_esatta'] . "</td>";
            echo "<td>";
            echo "<a class='btn btn-success' href=\"modifica.php?id=" . $domanda['id'] . "\">Modifica</a> ";
            echo "<a class='btn btn-warning' href=\"elimina.php?id=" . $domanda['id'] . "\">Elimina</a>";
            echo "</td>";
            echo "</tr>";
        }
        mysql_close($link);
        ?>
    </table>
    <div align="center">
        <a href="aggiungi.php" class="btn btn-success">Aggiungi una domanda</a>
    </div>
</div>
</body>